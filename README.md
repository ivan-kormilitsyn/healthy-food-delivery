# Healthy-food-delivery

SPA "Healthy food delivery"

## Getting started

To make it easy for you to get started my application, here's a list of recommended next steps.


```
git clone https://gitlab.com/ivan-kormilitsyn/healthy-food-delivery.git
```
```
cd healthy-food-delivery
```
```
npm install
```
```
json-server db.json
```
```
npm run dev
```